.PHONY: all build shell

all:
	@echo targets are \`build\` and \`shell\`

build:
	buildah bud -t goozbach/ansible -f container/Dockerfile .

shell:
	podman run --rm -it -v $$(pwd):/var/lib/ansible/:z goozbach/ansible /bin/bash
